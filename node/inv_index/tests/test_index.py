import os
import pickle

from ..build import InvertedIndexBuilder


def test_init():
    config = {"base_dir": os.path.dirname(os.path.realpath(__file__)), "max_doc_count": 2}
    builder = InvertedIndexBuilder(config)


def test_builder():
    config = {"base_dir": os.path.dirname(os.path.realpath(__file__)), "max_doc_count": 2}
    builder = InvertedIndexBuilder(config)

    builder.build()
    index = pickle.load(open(config["base_dir"] + "/index/inverted.index", "rb"))
    for key in index:
        assert len(index[key]) <= int(config["max_doc_count"])
